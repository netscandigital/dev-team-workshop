# README #

Dev Team Workshop - For training of dev team

# Requirements #
- Fedora 29 (Or another distro)
- Vagrant
- Homestead (Laravel)
- Git

# Steps #
## WARNING ##
Each time the master branch is updated (with new steps on this readme), you must:

- Checkout master
- Git pull
- Checkout your branch
- ```git rebase master```
## Getting started ##
- Clone repo
- Config Homestead.yml
- Config /etc/hosts
- ```vagrant reload --provision```
- Access http://workshop.test
## Configuring Composer ##
- Create a branch for you: ```git checkout -b **[your name as branch name]**```
- Access vagrant (vagrant ssh)
- Go to /home/vagrant/code/[project dir]
- ```composer init```
- Set this info:
    - *Package name:* keeva/dev-team-workshop
    - *Description:* Dev team workshop for training
    - *Author:* **[your name]** <**[your email]**>
    - *Minimum Stability:* **[just hit enter]**
    - *Package Type:* project
    - *License:* MIT
    - *Would you like to define your dependencies (require) interactively [yes]?* **no**
    - *Would you like to define your dev dependencies (require-dev) interactively [yes]?* **no**
    - *Do you confirm generation [yes]?* **yes**
    - *Would you like the vendor directory added to your .gitignore [yes]?* **yes**
- Run ```composer install```, it will generate vendor dir
- Add your changes: **git add .**
- Commit your changes: **git commit**
- Push your branch: **git push**
    - If your branch doesn't exists on remote server, create it: **git push -u origin [your branch]**
## Configuring Autoload (PSR-4) ##
- Edit **composer.json** file, add the autoload section after "require: {}", should looks like this:

---
```
{
    "name": "keeva/dev-team-workshop",
    ...
    "require": {},
    "autoload": {
        "classmap": [],
        "psr-4": {
            "App\\": "app/"
        }
    }
}
```
---

- Create the dir "app/Services"
- Create a "PHPInfo.php" file into "app/Services", put this content:

---
```
<?php

namespace App\Services;

class PHPInfo
{
    /**
     * Show PHP info
     * @return void
     */
    public function showInfo()
    {
        phpinfo();
    }
}
```
---

- Edit the "index.php" file, contents should looks like:

---
```
<?php

// Including autoload
require __DIR__ . 'vendor/autoload.php';

// Use PHPInfo service
use App\Services\PHPInfo;

// Show php info
$phpInfo = new PHPInfo();
$phpInfo->showInfo();

```
---

- Access http://workshop.test and see the changes
- If works, commit and push your changes into your branch
## MVC - Handling routes ##
- Given the scenario:
    - The 'workshop.test' have 3 (three) pages:
        - Home: workshop.test/
        - Contacts: workshop.test/fale-conosco
        - Products: workshop.test/produtos
    - This pages must be implemented with MVC Pattern, in this case we should have:
        - app/Controllers: Controllers dir
        - app/Views: Views dir
        - app/Models: Models dir
    - The pages MUST be Views
    - We MUST have the SiteController (app/Controllers/SiteController.php)
    - The Views must have the structure:
---
```
<?php

namespace App\Views;

class YourView
{
    /**
     * Show the view contents
     * @return string
     */
    public function getContents() : string
    {
        return 'My contents ;)';
    }
}
```
---
    - The Controllers must have the structure:
---
```
<?php

namespace App\Controllers;

class YourController
{
    /**
     * @const  array  ROUTES  Routes array
     */
    const ROUTES = [
        'uri' => 'view.namespace'
    ];

    /**
     * Return the right content for given URI
     * @param  string  $uri  URI
     * @return string
     */
    public function route(string $uri) : string
    {
        // Get the namespace from URI
        // Instance the View object
        // Return view getContents return
    }
}
```
    - index.php MUST instance the controller to handle the URI and return route return